<?php
	//Funcion que devuelve true si el numero es primo o false si no lo es.
	function primo($num)
	{
		//Definicion de numero primo:  Un numero primo es un número entero que solamente es divisible por él mismo

	    $contador=0;
	    //for que recorre todos los numeros desde el 2 hasta el valor recibido
	    for($i=2;$i<=$num;$i++)
	    {
	        if($num%$i==0)
	        {
	        	$contador++;
	            //si se divide por algun numero mas de una vez, no es primo
	            if($contador>1)
	                return false;
	        }
	    }
	    return true;
	}

	//Test de funcion primo:
	// $numeroPueba= 13;
	// if(primo($numeroPueba))
	// {
	// 	echo "Es primo";
	// }
	// else
	// {
	// 	echo "No es primo";
	// }


	//descompone un numero en digitos
	function descomponer($num)
	{
		$numeros = array();
		while($num != 0)
		{
			$numeros[] = $num % 10; //resto de la division
			$num = intval($num/10); //intval lo que hace es quitarle la parte decimal al resultado de la division
		}
		return $numeros;
	}

	//Test funcion descomponer:
	//$imprimir = descomponer(145672);
	//print_r($imprimir);

	//calcula el factorial de un numero determinado
	function factorial($num)
	{
		$cont= $num;
		$aux = $num;
		for ($i=1; $i < $cont; $i++) 
		{ 
			$aux= $aux-1;
			$num = $num*($aux);

		}
		return $num;
	}

	//Test funcion factorial
	//echo factorial(7);

	// function posibilidades($arreglo)
	// {
	// 	$combinaciones = array();
	// 	$lenght = count($arreglo); //cantidad de digitos del numero
	// 	$posibilidades = factorial($lenght); //cantidad de combinaciones posibles del numero
	// 	$resultado="";

	// 	for($i=0; $i<$posibilidades; $i++)
 //      	{
 //      		for ($j=0; $j < $lenght ; $j++) 
 //      		{
 //      			 $resultado= $resultado.$arreglo[$j];
 //      		}
 //      		$combinaciones[] = $resultado;
 //      		//aca hay que reordenar el arreglo
 //      		$resultado="";
 //      	}
 //      	print_r($combinaciones);
	// }
	// $num = array(1,2,3);
	// posibilidades($num);

	//funcion que recibe un arreglo de numeros y lo compone en un numero entero
	function componer($num)
	{
		$resultado=0;
		$multiplicador=1;
		for ($i= count($num); $i > 0; $i--) 
		{ 
			$resultado= $resultado+($num[$i-1]*$multiplicador);
			$multiplicador=$multiplicador*10;
		}
		return $resultado;
		
	}

	//problemas en la logica de la funcion (Revisar)
	function posibilidades ($arr)
	{
		$res = array();

		for ($i=0; $i < count($arr) ; $i++) 
		{ 
			for ($j=0; $j < count($arr); $j++) 
			{ 
				if($j != $i)
				{
					$aux = $arr;
					$aux[$j] = $arr[$i];
					$aux[$i] = $arr[$j];
					$res[]= componer($aux);
				}
			}
		}
		return $res;
		//print_r($res);
	}
	//Test funcion posibilidades:
	//$prueba = array(1,3,5,7);
	//posibilidades($prueba);


	//Esta funcion recibe un arreglo con todas las posibilidades de un numero y devuelve true si todas las posibilidades son primas o false si no lo son
	function esPrimoCircular($arr)
	{
		$res=true;
		$i=0;
		while ($i < count($arr) && $res) 
		{
			$res= primo($arr[$i]);
			$i++;
		}
		return $res;
	}

	// $arr = array(37,73);
	// $res= esPrimoCircular($arr);
	// if($res){
	// 	echo "es primo circular";
	// }
	// else{
	// 	echo "no es primo circular";
	// }


	//funcion que devuelve los primos circulares del uno al 100.
	function primoCircular()
	{
		$primosCirculares = array();
		for ($i=1; $i<=100 ; $i++)
		{
			$numDescompuesto =descomponer($i);
			$posibilidades = posibilidades($numDescompuesto);
			if (esPrimoCircular($posibilidades))
			{
				$primosCirculares[] = $i;
			}

		}
		print_r($primosCirculares);
		//return $primosCirculares;
	}
	primoCircular();

?>